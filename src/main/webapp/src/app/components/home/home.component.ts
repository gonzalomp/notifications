import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  public username: String = '';
  public userSubscribe: boolean = false;
  private reader: any = '';

  constructor(
    public notificationService: NotificationService
  ) { }

  ngOnInit(): void {
  }


  subscribe() {
    console.log("Se ha suscrito: "+ this.username);
    this.notificationService.subscribeUser(this.username).then(
      response => {
        this.reader = response;
        this.userSubscribe = true;
      }
    );
    
  }

  unsubscribe() {
    console.log(this.username + " ha cancelado su suscripción");
    this.notificationService.unsubscribeUser(this.reader.id);
    this.userSubscribe = false;
    this.username = '';
  }


}
