import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.scss']
})
export class PublisherComponent implements OnInit {

  public notification: String = '';
  public subscribers: any;

  constructor(
    public notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.getSubscribers();
  }


  sendNotification(){
    console.log("Enviamos la notificación: " + this.notification);
    this.notificationService.sendNotification(this.notification);
    this.notification = '';
  }


  getSubscribers(){
    console.log("Intentamos recuperar los suscriptores");
   
    this.notificationService.getSubscribers().then(
      response => {
        this.subscribers = response;
        console.log(this.subscribers);
      }
    );;
  }


  refreshSubscribers(){
    this.getSubscribers();
  }

}
