import { Injectable } from '@angular/core';
import * as StompJs from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { HttpClient } from '@angular/common/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notifications: String;
  stompClient: any;

  constructor(
    private http: HttpClient
  ) {
    this.connect();
    this.notifications = '';
    
  }
   
   connect() {
    this.stompClient =  StompJs.Stomp.over(new SockJS('http://localhost:8080/mywebsocket'));
    this.stompClient.connect({}, () => {
        console.log('Connected: ');
        this.stompClient.subscribe("/channel/notifications", notification => {
          this.notifications = this.notifications + "> " + notification.body + "</br>"
        });
    });

  }  

  getHttpHeader(contentType?: string): HttpHeaders {
    if (contentType) {
      return new HttpHeaders({
        'Content-Type': contentType
      });
    } else {
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }

  public sendNotification(notification: String) {
    const endpoint = "http://localhost:8080/sendNotification";
    const header = this.getHttpHeader();
    return this.http.post(endpoint, notification, { headers: header,  observe: 'response' }).toPromise().then(
        resultOk => {
            console.log("Respuesta correcta sendNotification");
        },
        resultError => {
          console.log("Respuesta erronea sendNotification");
        }
    );
  }


  public  subscribeUser(name: String) {
    const endpoint = "http://localhost:8080/subscribeUser";
    const header = this.getHttpHeader();
    return this.http.post(endpoint, name, { headers: header,  observe: 'response' }).toPromise().then(
        resultOk => {
            console.log("Respuesta correcta subscribeUser");
            return resultOk.body;
        },
        resultError => {
          console.log("Respuesta erronea subscribeUser");
          console.log(resultError);
          return '';
        }
    );
  }

  public unsubscribeUser(id: String) {
    const endpoint = "http://localhost:8080/unsubscribeUser";
    const header = this.getHttpHeader();
    return this.http.post(endpoint, id, { headers: header, observe: 'response' }).toPromise().then(
        resultOk => {
            console.log("Respuesta correcta unsubscribeUser");
        },
        resultError => {
          console.log("Respuesta erronea unsubscribeUser");
        }
    );
  }

  public getSubscribers() {
    const endpoint = "http://localhost:8080/subscribers";
    const header = this.getHttpHeader();
    return this.http.get(endpoint, { headers: header,  observe: 'response' }).toPromise().then(
        resultOk => {
            console.log("Respuesta correcta subscribers");
            return resultOk.body;
        },
        resultError => {
          console.log("Respuesta erronea subscribers");
        }
    );
  }

}