package com.sdg.gmp.notifications.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sdg.gmp.notifications.model.Reader;
import com.sdg.gmp.notifications.service.NotificationService;

@RestController
@CrossOrigin
public class MyController {

	@Autowired
    private NotificationService notificatonService;
	
	
	@RequestMapping(value = "/sendNotification", method = RequestMethod.POST)
	public void sendNotification(@RequestBody String notification) throws Exception {
		notificatonService.sendNotification(notification);
	}
	
	@RequestMapping(value = "/subscribeUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Reader subscribeUser(@RequestBody String name) throws Exception {
		return notificatonService.subscribeReader(name);
	}
	
	@RequestMapping(value = "/unsubscribeUser", method = RequestMethod.POST)
	public void unsubscribeUser(@RequestBody String id) throws Exception {
		notificatonService.unsubscribeReader(id);
	}

	@RequestMapping(value = "/subscribers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Reader> getSubscribers() throws Exception {
		return notificatonService.getReaders();
		
	}
	
	
	 

}