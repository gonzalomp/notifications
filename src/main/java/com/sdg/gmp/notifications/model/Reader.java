package com.sdg.gmp.notifications.model;

import java.util.UUID;


public class Reader {
	
	private UUID id;
	private String name; 
	
	
	
	public Reader() {
		this.id = UUID.randomUUID();
	}
	
	public Reader(String name) {
		this.id = UUID.randomUUID();
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Reader [id=" + id + ", name=" + name + "]";
	}
	
	
	

}
