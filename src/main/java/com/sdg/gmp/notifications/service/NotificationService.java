package com.sdg.gmp.notifications.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.sdg.gmp.notifications.model.Reader;

@Service
public class NotificationService {
	
	private final static Logger logger = Logger.getLogger(NotificationService.class.getName());
	
	@Autowired
	private SimpMessagingTemplate template;
	
    private  Map<String, Reader> readers = new HashMap<>();

    public List<Reader> getReaders() {
    	List<Reader> listReaders = new ArrayList<Reader>(readers.values());
    	logger.log(Level.INFO, "Recuperando listado de usuarios: "+listReaders);
    	return listReaders;
		
	}

	public Reader subscribeReader(String name) {
    	Reader reader = new Reader(name);
    	readers.put(reader.getId().toString(), reader);
    	logger.log(Level.INFO, "Se ha suscrito al canal : "+reader);
    	return reader;
    }
    
    public void unsubscribeReader(String id) {
    	
    	if (readers.containsKey(id)) {
    		Reader reader = readers.get(id);
    		readers.remove(id);
        	logger.log(Level.INFO, "Ha cancelado su subscripción: "+reader);
    	}else {
        	logger.log(Level.WARNING, "No se puede cancelar la suscrición del  usuario con id : "+id+" al no ser un identificador válido");
    	}
    	
    }
    
    
    public void sendNotification(String notification) {
    	try {
    		this.template.convertAndSend("/channel/notifications", notification);
    		logger.log(Level.INFO, "Se ha enviado al canal /channel/notifications la notificación: "+notification);
    		
    	} catch  (MessagingException e) {
    		logger.log(Level.SEVERE, "Se  ha producido un error enviando la notificacion: "+e.getMessage());
    	}
    	
    }
    
    

	
	
	
	

}
