# Notifications

La aplicación consiste en un sistema de notificaciones en tiempo real que permite a los usuarios suscribirse al canal para ver en directo las notificaciones enviadas por un usuario publicador o cancelar su suscripción. 
El usuario publicador además puede consultar el listado de suscriptores en un momento dado.

La aplicación se basa en la utilización de websockets y está formada por una parte back implementada en Java 8 y una parte front realizada en Angular 11 que nos permite validar el funcionamiento.


### Pre-requisitos 📋
Las versiones con las que hemos probado la aplicación son: 

* Java 8
* Node 14.16.0
* Angular 11




### Instalación 🔧

_Descargamos el código del directorio de [Bitbucket](https://bitbucket.org/gonzalomp/notifications/src/master/)_ 

_Sobre el directorio raíz del proyecto ejecutamos maven para descargar dependencias de back_

```
mvn clean install
```

_Nos situamos en el directorio /src/main/webapp donde se encuentra la vista_

```
cd src/main/webapp
```

_Hacemos un npm install para descargar las dependencias de vista_

```
npm install
```



## Despliegue 📦


_Sobre el directorio raíz ejecutamos la parte back con springboot_

```
mvn spring-boot:run
```

_Nos situamos en el directorio src/main/webapp para lanzar el front. Con el parametro -o ya se nos abrirá una ventana de navegador con la aplicación_

```
ng serve -o
```

_Para hacer pruebas con distintos roles debemos abrir ventanas de incógnito o realizarlas desde otros navegadores_



## Autor ✒️


* **Gonzalo Montoiro Pereiro**- [Bitbucket](https://bitbucket.org/gonzalomp)


.


